# What is this
Ｔｏ　ｍａｋｅ　ｙｏｕｒ　ｋｅｙｂｏａｒｄ　ｔｙｐｅ　ｌｉｋｅ　ｔｈｉｓ．  
Ｉ　ｕｓｅ　Ｕ３０００　ｆｏｒ　ｓｐａｃｅ．  
Ｔｈｅｒｅ＇ｓ　ｎｏ　ＵＦＦｘｘ　ｍａｐ　ｆｏｒ　ｗｈｉｔｅｓｐａｃｅ．  

# Instruction

Copy this file to your xkb layout directory.  
Mine is: `/usr/share/X11/xkb/symbols`

Either:
* `setxkbmap`
* `localectl`
* whatever work for you

# Question
> Why don't you name this repository ｘｋｂ－ｆｕｌｌｗｉｄｔｈ?

So you can clone them easily.
